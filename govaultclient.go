package govaultclient

import (
	"fmt"
	"io/ioutil"
	"log"

	api "github.com/hashicorp/vault/api"
)

type Secret struct {
	Path  string
	Key   string
	Value string
}

func GetJwtToken(jwtPath string) (string, error) {
	buf, err := ioutil.ReadFile(jwtPath)
	if err != nil {
		return "", err
	}

	return string(buf), err
}

func Login(vaultAddr, jwtPath, kubeAuthRole, kubeAuthPath string) (*api.Client, error) {
	log.Printf("Conneting to vault using address %s...\n", vaultAddr)

	config := &api.Config{
		Address: vaultAddr,
	}

	client, err := api.NewClient(config)
	if err != nil {
		log.Printf("Failed to connect to vault using %s: %v\n", vaultAddr, err)
		return nil, err
	}

	jwtToken, err := GetJwtToken(jwtPath)
	if err != nil {
		log.Printf("Failed to retrieve jwt token: %v\n", err)
		return nil, err
	}

	body := map[string]string{
		"role": kubeAuthRole,
		"jwt":  jwtToken,
	}

	loginPath := fmt.Sprintf("/v1/auth/%s/login", kubeAuthPath)
	log.Printf(
		"Will attempt to login to vault using \n\tpath: %s\n\trole: %s\n\tjwt: %s\n",
		kubeAuthPath,
		kubeAuthRole,
		jwtPath,
	)

	req := client.NewRequest("POST", loginPath)
	req.SetJSONBody(body)

	resp, err := client.RawRequest(req)
	if err != nil {
		log.Printf("Failed to login to vault using %s\n", req.URL.String())
		return nil, err
	}

	if respErr := resp.Error(); respErr != nil {
		log.Printf("Api error: %v\n", respErr)
		return nil, respErr
	}

	var result api.Secret
	if err := resp.DecodeJSON(&result); err != nil {
		log.Printf("Could not decode JSON response: %v\n", err)
		return nil, err
	}

	log.Printf("Vault login was successful %+v\n", result)

	client.SetToken(result.Auth.ClientToken)

	return client, nil
}

func GetSecrets(secretPath string, client *api.Client) ([]Secret, error) {
	resp, err := client.Logical().Read(secretPath)
	if err != nil {
		log.Printf("Failed to get secret from path %s\n", secretPath)
		return nil, err
	}

	if resp == nil {
		log.Printf("No entry found at %s\n", secretPath)
		return nil, err
	}

	data := resp.Data
	if resp.Data["data"] != nil {
		data = resp.Data["data"].(map[string]interface{})
	}

	log.Printf("Found %d entries at path %s\n", len(data), secretPath)

	results := make([]Secret, 0)
	for k, v := range data {
		strVal := v.(string)
		secretEntry := Secret{
			Path:  secretPath,
			Key:   k,
			Value: strVal,
		}
		results = append(results, secretEntry)
	}

	return results, nil
}
